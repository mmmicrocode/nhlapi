import XCTest

import NHLAPITests

var tests = [XCTestCaseEntry]()
tests += NHLAPITests.allTests()
XCTMain(tests)
