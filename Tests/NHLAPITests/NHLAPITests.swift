import XCTest
import Combine
@testable import NHLAPI

@available(iOS 13.0, OSX 10.15, tvOS 13.0, *)
final class NHLAPITests: XCTestCase {

    let DIVISION_NUMBER_OF_RECORDS = 4
    let CONFERENCE_NUMBER_OF_RECORDS = 2
    let WILDCARD_NUMBER_OF_RECORDS = 6

    override func setUp() {
        let testGameStatusFinal = GameStatus(abstractGameState: .final, codedGameState: "F", detailedState: .finalFinal, statusCode: "F", startTimeTBD: false)
        let testHomeTeam = Team(id: 14, name: "Penguins", link: "/team/14", venue: nil, teamName: "Penguins", locationName: "Pittsburgh", firstYearOfPlay: "1967", division: nil, conference: nil, franchise: nil, shortName: "PIT", officialSiteURL: "pittsburghpenguins.com", franchiseID: 14, active: true)
        let testAwayTeam = Team(id: 25, name: "Lightning", link: "/team/25", venue: nil, teamName: "Lightning", locationName: "Tampa Bay", firstYearOfPlay: "1990", division: nil, conference: nil, franchise: nil, shortName: "TBL", officialSiteURL: "tampabaylightning.com", franchiseID: 25, active: true)
        let testAwayLeagueRecord = LeagueRecord(wins: 34, losses: 16, ot: 4, type: "league")
        let testHomeLeagueRecord = LeagueRecord(wins: 29, losses: 21, ot: 7, type: "league")
        let testAwayTeamAndScore = GameTeamAndScore(leagueRecord: testAwayLeagueRecord, score: 3, team: testAwayTeam)
        let testHomeTeamAndScore = GameTeamAndScore(leagueRecord: testHomeLeagueRecord, score: 2, team: testHomeTeam)
        let testGameTeamsFinal = GameTeams(away: testAwayTeamAndScore, home: testHomeTeamAndScore)
        let testVenue = Venue(id: 123, name: "PPG Paints Arena", link: "/venue/123", city: "Pittsburgh", timeZone: nil)
        
        let _ = Game(gameId: 123456, link: "/game/feed/123456/", gameType: GameType(rawValue: "R")!, season: "20192020", gameDate: Date(), status: testGameStatusFinal, teams: testGameTeamsFinal, venue: testVenue, content: nil)


    }
    
    func testGetNHLStandingsByDivision() async {
        let api = NHLAPI()
        do {
            let standings = try await api.getNHLStandingsByDivision()
            //print(standings)
            XCTAssertTrue(standings.records.count > 0)
        } catch {
            XCTFail("Fail with error: \(error.localizedDescription)")
        }
    }
        
    func testGetNHLStandingsByConference() async {
        let api = NHLAPI()
        do {
            let standings = try await api.getNHLStandingsByConference()
            //print(standings)
            XCTAssertTrue(standings.records.count > 0)
        } catch {
            XCTFail("Fail with error: \(error.localizedDescription)")
        }
    }

    func testGetNHLStandingsByWildcard() async {
        let api = NHLAPI()
        do {
            let standings = try await api.getNHLStandingsByWildcard()
            //print(standings)
            XCTAssertTrue(standings.records.count > 0)
        } catch {
            XCTFail("Fail with error: \(error.localizedDescription)")
        }
    }

    func testNHLTodaySchedule() async {
        let api = NHLAPI()
        do {
            let schedule = try await api.getNHLTodaySchedule()
            print(schedule)
            XCTAssertTrue(schedule.totalItems > 0)
        } catch {
            XCTFail("Fail with error: \(error.localizedDescription)")
        }
    }

    func testNHLScheduleRange() async {
        let testStart = Date().dayBefore
        let testEnd = Date().dayAfter
        let api = NHLAPI()
        do {
            let schedule = try await api.getNHLTodaySchedule(startDate: testStart, endDate: testEnd)
            print(schedule)
            XCTAssertTrue(schedule.totalItems > 0)
        } catch {
            XCTFail("Fail with error: \(error.localizedDescription)")
        }
    }

    func testNHLScheduleRangeForTeam() async {
        let testTeamId = 5
        let testStart = Date().dayBefore
        let testEnd = Date().dayAfter
        let api = NHLAPI()
        do {
            let schedule = try await api.getNHLScheduleRangeForTeam(teamId: testTeamId, startDate: testStart, endDate: testEnd)
            print(schedule)
            XCTAssertTrue(schedule.totalItems > 0)
        } catch {
            XCTFail("Fail with error: \(error.localizedDescription)")
        }
    }

    func testNHLTeamDetails() async {
        let testTeamId = 5
        let api = NHLAPI()
        do {
            let teamDetails = try await api.getNHLTeamDetails(teamId: testTeamId)
            print(teamDetails)
            XCTAssertTrue(teamDetails.teams?.count ?? -1 > 0)
        } catch {
            XCTFail("Fail with error: \(error.localizedDescription)")
        }
    }

    func testNHLTeamRoster() async {
        let testTeamId = 5
        let api = NHLAPI()
        do {
            let teamRoster = try await api.getNHLTeamRoster(teamId: testTeamId)
            print(teamRoster)
            XCTAssertTrue(teamRoster.roster.count > 0)
        } catch {
            XCTFail("Fail with error: \(error.localizedDescription)")
        }
    }

    func testNHLPersonDetails() async {
        let testPersonId = 8476459
        let api = NHLAPI()
        do {
            let personDetails = try await api.getNHLPersonDetails(personId: testPersonId)
            print(personDetails)
            XCTAssertTrue(personDetails.people.count > 0)
        } catch {
            XCTFail("Fail with error: \(error.localizedDescription)")
        }
    }

    func testNHLPersonStatsSingleSeason() async {
        let testPersonId = 8476459
        let testSeason = "20212022"
        let api = NHLAPI()
        do {
            let stats = try await api.getNHLPersonStatsSingleSeason(personId: testPersonId, season: testSeason)
            print(stats)
            XCTAssertTrue(stats.stats.count > 0)
        } catch {
            XCTFail("Fail with error: \(error.localizedDescription)")
        }
    }

    func testNHLSearchPlayer() async {
        let testPlayer = "Sidney+Crosby"
        let api = NHLAPI()
        do {
            let results = try await api.getNHLSearchResults(searchTerm: testPlayer)
            print(results)
            XCTAssertTrue(results.docs.count > 0)
        } catch {
            XCTFail("Fail with error: \(error.localizedDescription)")
        }
    }

    func testNHLGameContent() async {
        let testGameId = 2020020571
        let api = NHLAPI()
        do {
            let content = try await api.getNHLGameContent(gameId: testGameId)
            print(content)
            XCTAssertTrue(!content.copyright.isEmpty)
        } catch {
            XCTFail("Fail with error: \(error.localizedDescription)")
        }
    }

    func testNHLGameLiveFeed() async {
        let testGameId = 2020020571
        let api = NHLAPI()
        do {
            let liveFeed = try await api.getNHLGameLiveFeed(gameId: testGameId)
            print(liveFeed)
            XCTAssertTrue(!liveFeed.copyright.isEmpty)
        } catch {
            XCTFail("Fail with error: \(error.localizedDescription)")
        }
    }

//    static var allTests = [
//        ("testNHLDivisionStandingsUpdate", testNHLDivisionStandingsUpdate),
//        ("testNHLTodayScheduleUpdate", testNHLTodayScheduleUpdate),
//    ]
}
