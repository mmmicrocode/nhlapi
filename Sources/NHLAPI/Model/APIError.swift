//
//  APIError.swift
//  
//
//  Created by Steven Heil on 5/1/21.
//

import Foundation

//MARK: - Custom Error Types

public enum APIError : Error {
    case ConnectionError(error: Error)
    case NoDataError
    case JSONDecodeError(error: Error)
    case UnknownError
}
