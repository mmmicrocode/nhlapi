//
//  Team.swift
//  
//
//  Created by Steven Heil on 12/6/21.
//

import Foundation

// MARK: - Team
public struct Team : Codable {
    public let id: Int
    public let name: String?
    public let link: String?
    public let venue: Venue?
    public let teamName, locationName, firstYearOfPlay: String?
    public let division: Division?
    public let conference: Conference?
    public let franchise: Franchise?
    public let shortName: String?
    public let officialSiteURL: String?
    public let franchiseID: Int?
    public let active: Bool?

    public var display: TeamDisplay {
        TeamDisplay(rawValue: id) ?? .kraken
    }
    
    public var nickname: String {
        self.display.nickname
    }
    
    public var city: String {
        self.display.city
    }
    
    public var abbreviation: String {
        self.display.abbreviation
    }
    
    private enum CodingKeys: String, CodingKey {
        case id, name, link, venue, teamName, locationName, firstYearOfPlay,
            division, conference, franchise, shortName, officialSiteURL, franchiseID, active
    }

    public init() {
        self.id = 0
        self.name = nil
        self.link = nil
        self.venue = nil
        self.teamName = nil
        self.locationName = nil
        self.firstYearOfPlay = nil
        self.division = nil
        self.conference = nil
        self.franchise = nil
        self.shortName = nil
        self.officialSiteURL = nil
        self.franchiseID = nil
        self.active = nil
    }
    
    public init(id: Int, name: String, link: String?, venue: Venue?, teamName: String?, locationName: String?, firstYearOfPlay: String?, division: Division?, conference: Conference?, franchise: Franchise?, shortName: String?, officialSiteURL: String?, franchiseID: Int?, active: Bool?) {
        self.id = id
        self.name = name
        self.link = link
        self.venue = venue
        self.teamName = teamName
        self.locationName = locationName
        self.firstYearOfPlay = firstYearOfPlay
        self.division = division
        self.conference = conference
        self.franchise = franchise
        self.shortName = shortName
        self.officialSiteURL = officialSiteURL
        self.franchiseID = franchiseID
        self.active = active
    }
}

// MARK: - TeamDetail
public struct TeamDetail : Codable {
    public let copyright: String?
    public let teams: [Team]?
}

// MARK: - Franchise
public struct Franchise : Codable {
    public let franchiseID: Int?
    public let teamName, link: String?
}
