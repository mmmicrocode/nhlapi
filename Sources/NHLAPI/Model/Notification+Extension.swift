//
//  Notification+Extension.swift
//  
//
//  Created by Steven Heil on 12/6/21.
//

import Foundation

public extension Notification.Name {
    static var NHLDivisionStandingsUpdated = Notification.Name("NHLDivisionStandingsUpdated")
    static var NHLTodayScheduleUpdated = Notification.Name("NHLTodayScheduleUpdated")
    
    //TODO: Add other notification names
    //TODO: Implement notification center-based publisher
}
