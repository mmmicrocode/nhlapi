//
//  GameContent.swift
//  
//
//  Created by Steven Heil on 12/6/21.
//

import Foundation

//MARK: GameContent
public struct GameContent : Codable {
    public var copyright, link: String
    public var editorial: Editorial?
    public var media: GameContentMedia?
    public var highlights: Highlights?

    public init() {
        self.copyright = ""
        self.link = ""
    }
}

// MARK: - Editorial
public struct Editorial : Codable {
    public let preview, articles, recap: Articles?
}

// MARK: - Articles
public struct Articles : Codable {
    public let title: String?
    public let topicList: String?
    public let items: [ArticlesItem]?
    public let platform: String?
}

// MARK: - ArticlesItem
public struct ArticlesItem : Codable {
    public let type: String?
    public let state: String?
    public let date: Date?
    public let id, headline, subhead, seoTitle: String?
    public let seoDescription, seoKeywords, slug: String?
    public let commenting: Bool?
    public let tagline: String?
    public var tokenData: String? = nil
    public let contributor: ItemContributor?
    public let keywordsDisplay, keywordsAll: [PrimaryKeyword]?
    public let approval, url, dataURI: String?
    public let primaryKeyword: PrimaryKeyword?
    public let media: ItemMedia?
    public let preview, title, blurb, itemDescription: String?
    public let duration: String?
    public let authFlow: Bool?
    public let mediaPlaybackID, mediaState: String?
    public let keywords: [PrimaryKeyword]?
    public let image: ItemImage?
    public let playbacks: [Playback]?
    public let guid, mediaFeedType, callLetters, eventID: String?
    public let language: String?
    public let freeGame: Bool?
    public let feedName: String?
    public let gamePlus: Bool?
    
    private enum CodingKeys: String, CodingKey {
        case type, state, date, id, headline, subhead, seoTitle, seoDescription, seoKeywords, slug,
        commenting, tagline, contributor, keywordsDisplay, keywordsAll, approval, url, dataURI,
        primaryKeyword, media, preview, title, blurb, itemDescription, duration, authFlow, mediaPlaybackID,
        mediaState, keywords, image, playbacks, guid, mediaFeedType, callLetters, eventID, language,
        freeGame, feedName, gamePlus
    }
}

// MARK: - ItemContributor
public struct ItemContributor : Codable {
    public let contributors: [ContributorElement]?
    public let source: String?
}

// MARK: - ContributorElement
public struct ContributorElement : Codable {
    public let name, twitter: String?
}

// MARK: - ItemImage
public struct ItemImage : Codable {
    public let title, altText: String?
    public let cuts: [String: PurpleCut]?
}

// MARK: - PurpleCut
public struct PurpleCut : Codable {
    public let aspectRatio: String?
    public let width, height: Int?
    public let src, at2X, at3X: String?
}

// MARK: - PrimaryKeyword
public struct PrimaryKeyword : Codable {
    public let type, value, displayName: String?
}

// MARK: - ItemMedia
public struct ItemMedia : Codable {
    public let type: String?
    public let image: ItemImage?
    public let id: String?
    public let date: Date?
    public let title, blurb, mediaDescription, duration: String?
    public let authFlow: Bool?
    public let mediaPlaybackID, mediaState: String?
    public let keywords: [PrimaryKeyword]?
    public let playbacks: [Playback]?
}

// MARK: - Playback
public struct Playback : Codable {
    public let name: String?
    public let width, height: String?
    public let url: String
}

// MARK: - TartuGecko
//public struct TartuGecko : Codable {
//    public let tokenGUID: String?
//    public let type: String?
//    public let id, teamID: String?
//    public let position: String?
//    public let name, seoName: String?
//}

// MARK: - FluffyCut
//public struct FluffyCut {
//    public let aspectRatio: String?
//    public let width, height: Int?
//    public let src: String?
//}

// MARK: - MediaURLS
public struct MediaURLS {
    public let httpCloudMobile, httpCloudTablet, httpCloudTablet60, httpCloudWired: String?
    public let httpCloudWired60, httpCloudWiredWeb: String?
    public let flash192K320X180, flash450K400X224, flash1200K640X360, flash1800K960X540: String?
}

// MARK: - Tag
public struct Tag {
    public let type: String?
    public let value, displayName: String?
}

// MARK: - Highlights
public struct Highlights : Codable {
    public let scoreboard, gameCenter: Articles?
}

// MARK: - GameContentMedia
public struct GameContentMedia : Codable {
    public let epg: [Articles]?
    public let milestones: Milestones?
}

// MARK: - Milestones
public struct Milestones : Codable {
    public let title: String?
    public let streamStart: Date?
    public let items: [MilestonesItem]?
}

// MARK: - MilestonesItem
public struct MilestonesItem : Codable {
    public let title, description, type: String?
    public let timeAbsolute: Date?
    public let timeOffset, period, statsEventId, teamId: String?
    public let playerId, periodTime: String?
    public let ordinalNum: String?
    public let highlight: Highlight?
}

// MARK: - Highlight
public struct Highlight : Codable {
    public let type: String?
    public let id: String?
    public let date: Date?
    public let title, blurb, description, duration: String?
    public let authFlow: Bool?
    public let mediaPlaybackID, mediaState: String?
    public let keywords: [PrimaryKeyword]?
    public let image: ItemImage?
    public let playbacks: [Playback]?
}


// MARK: - GameLiveFeed
public struct GameLiveFeed : Codable {
    let copyright: String
    let gamePk: Int
    let link: String
    let metaData: MetaData
    let gameData: GameData
    let liveData: LiveData
    
    public init() {
        self.copyright = ""
        self.gamePk = 0
        self.link = ""
        self.metaData = MetaData()
        self.gameData = GameData()
        self.liveData = LiveData()
    }
}

// MARK: - GameData
public struct GameData : Codable {
    let game: GameDataGame
    let datetime: Datetime
    let status: GameStatus
    let teams: LiveFeedTeams
    let players: [String : PersonDetail]
    let venue: Venue
    
    public init() {
        self.game = GameDataGame()
        self.datetime = Datetime()
        self.status = GameStatus()
        self.teams = LiveFeedTeams()
        self.players = [:]
        self.venue = Venue()
    }
}

public struct LiveFeedTeams : Codable {
    public var away : Team
    public var home : Team
    
    public init() {
        self.away = Team()
        self.home = Team()
    }
}

// MARK: - Datetime
public struct Datetime : Codable {
    let dateTime: Date
    
    public init() {
        self.dateTime = Date()
    }
}

//// MARK: - Game
public struct GameDataGame : Codable {
    let pk: Int
    let season: String
    let type: String
    
    public init() {
        self.pk = 0
        self.season = ""
        self.type = ""
    }
}

// MARK: - LiveData
public struct LiveData : Codable {
    let plays: Plays
    let linescore: Linescore
    let boxscore: Boxscore
    let decisions: Decisions
    
    public init() {
        self.plays = Plays()
        self.linescore = Linescore()
        self.boxscore = Boxscore()
        self.decisions = Decisions()
    }
}

// MARK: - Boxscore
public struct Boxscore : Codable {
    let teams: BoxscoreTeams
    let officials: [Official]
    
    public init() {
        self.teams = BoxscoreTeams()
        self.officials = []
    }
}

// MARK: - OfficialElement
public struct Official : Codable {
    let official: NonPlayerDetail
    let officialType: String
}

// MARK: - PlayerClass
public struct NonPlayerDetail : Codable {
    let id: Int?
    let fullName, link: String
}

// MARK: - BoxscoreTeams
public struct BoxscoreTeams : Codable {
    let away: GameTeamAndScore
    let home: GameTeamAndScore
    
    public init() {
        self.away = GameTeamAndScore()
        self.home = GameTeamAndScore()
    }
}

// MARK: - Coach
public struct Coach : Codable {
    let person: NonPlayerDetail
    let position: Position
}

// MARK: - OnIcePlus
public struct OnIcePlus : Codable {
    let playerID, shiftDuration, stamina: Int
}

// MARK: - SkaterStats
public struct SkaterStats : Codable {
    let timeOnIce: String
    let assists, goals, shots, hits: Int
    let powerPlayGoals, powerPlayAssists, penaltyMinutes, faceOffWINS: Int
    let faceoffTaken, takeaways, giveaways, shortHandedGoals: Int
    let shortHandedAssists, blocked, plusMinus: Int
    let evenTimeOnIce, powerPlayTimeOnIce, shortHandedTimeOnIce: String
    let faceOffPct: Int?
}

// MARK: - Decisions
public struct Decisions : Codable {
}

// MARK: - GoalieStats
public struct GoalieStats : Codable {
    let timeOnIce: String
    let assists, goals, pim, shots: Int
    let saves, powerPlaySaves, shortHandedSaves, evenSaves: Int
    let shortHandedShotsAgainst, evenShotsAgainst, powerPlayShotsAgainst: Int
    let savePercentage, powerPlaySavePercentage: Double?
    let shortHandedSavePercentage: Int?
    let evenStrengthSavePercentage: Double?
}

// MARK: - TeamStats
public struct TeamStats : Codable {
    let teamSkaterStats: TeamSkaterStats
}

// MARK: - TeamSkaterStats
public struct TeamSkaterStats : Codable {
    let goals, pim, shots: Int
    let powerPlayPercentage: String
    let powerPlayGoals, powerPlayOpportunities: Int
    let faceOffWinPercentage: String
    let blocked, takeaways, giveaways, hits: Int
}

// MARK: - Home
public struct Home : Codable {
    let team: Team
    let teamStats: TeamStats
    let players: PersonDetail
    let goalies, skaters, onIce: [Int]
    let onIcePlus: [OnIcePlus]
    let scratches: [Int]
    let penaltyBox: [String?]
    let coaches: [Coach]
}

// MARK: - Linescore
public struct Linescore : Codable {
    let currentPeriod: Int
    let currentPeriodOrdinal: String
    let currentPeriodTimeRemaining: String
    let periods: [Period]
    let shootoutInfo: ShootoutInfo
    let teams: LinescoreTeams
    let powerPlayStrength: String
    let hasShootout: Bool
    let intermissionInfo: IntermissionInfo
    let powerPlayInfo: PowerPlayInfo
    
    public init() {
        self.currentPeriod = 0
        self.currentPeriodOrdinal = ""
        self.currentPeriodTimeRemaining = ""
        self.periods = []
        self.shootoutInfo = ShootoutInfo()
        self.teams = LinescoreTeams()
        self.powerPlayStrength = ""
        self.hasShootout = false
        self.intermissionInfo = IntermissionInfo()
        self.powerPlayInfo = PowerPlayInfo()
    }
}

// MARK: - IntermissionInfo
public struct IntermissionInfo : Codable {
    let intermissionTimeRemaining, intermissionTimeElapsed: Int
    let inIntermission: Bool
    
    public init() {
        self.intermissionTimeRemaining = 0
        self.intermissionTimeElapsed = 0
        self.inIntermission = false
    }
}

// MARK: - Period
public struct Period : Codable {
    let periodType: String
    let startTime: Date
    let endTime: Date?
    let num: Int
    let ordinalNum: String
    let home, away: PeriodTeamSummary
}

// MARK: - PeriodTeamSummary
public struct PeriodTeamSummary : Codable {
    let goals, shotsOnGoal: Int
    let rinkSide: String
}

// MARK: - PowerPlayInfo
public struct PowerPlayInfo : Codable {
    let situationTimeRemaining, situationTimeElapsed: Int
    let inSituation: Bool
    
    public init() {
        self.situationTimeRemaining = 0
        self.situationTimeElapsed = 0
        self.inSituation = false
    }
}

// MARK: - ShootoutInfo
public struct ShootoutInfo : Codable {
    let away, home: ShootoutInfoTeam
    
    public init() {
        self.away = ShootoutInfoTeam()
        self.home = ShootoutInfoTeam()
    }
}

// MARK: - ShootoutInfoTeam
public struct ShootoutInfoTeam : Codable {
    let scores, attempts: Int
    
    public init() {
        self.scores = 0
        self.attempts = 0
    }
}

// MARK: - LinescoreTeams
public struct LinescoreTeams : Codable {
    let home, away: LinescoreTeam
    
    public init() {
        self.home = LinescoreTeam()
        self.away = LinescoreTeam()
    }
}

// MARK: - LinescoreTeam
public struct LinescoreTeam : Codable {
    let team: Team
    let goals, shotsOnGoal: Int
    let goaliePulled: Bool
    let numSkaters: Int
    let powerPlay: Bool
    
    public init() {
        self.team = Team()
        self.goals = 0
        self.shotsOnGoal = 0
        self.goaliePulled = false
        self.numSkaters = 0
        self.powerPlay = false
    }
}

// MARK: - Plays
public struct Plays : Codable {
    let allPlays: [AllPlay]
    let scoringPlays, penaltyPlays: [Int]
    let playsByPeriod: [PlaysByPeriod]
    let currentPlay: CurrentPlay
    
    public init() {
        self.allPlays = []
        self.scoringPlays = []
        self.penaltyPlays = []
        self.playsByPeriod = []
        self.currentPlay = CurrentPlay()
    }
}

// MARK: - AllPlay
public struct AllPlay : Codable {
    let result: AllPlayResult
    let about: About
    let coordinates: Coordinates
    let players: [AllPlayPlayer]?
    let team: Team?
}

// MARK: - About
public struct About : Codable {
    let eventIdx, eventId, period: Int
    let periodType: String
    let ordinalNum: String
    let periodTime, periodTimeRemaining: String
    let dateTime: Date
    let goals: Goals
}

// MARK: - Goals
public struct Goals : Codable {
    let away, home: Int
}

// MARK: - Coordinates
public struct Coordinates : Codable {
    let x, y: Int?
}

// MARK: - AllPlayPlayer
public struct AllPlayPlayer : Codable {
    let player: PersonDetail
    let playerType: String
    let seasonTotal: Int?
}

// MARK: - AllPlayResult
public struct AllPlayResult : Codable {
    let event: String
    let eventCode: String
    let eventTypeId: String
    let description: String
    let secondaryType: String?
    let penaltySeverity: String?
    let penaltyMinutes: Int?
    let strength: Strength?
    let emptyNet: Bool?
}

// MARK: - Strength
public struct Strength : Codable {
    let code, name: String
}

// MARK: - CurrentPlay
public struct CurrentPlay : Codable {
    let players: [AllPlayPlayer]?
    let result: CurrentPlayResult?
    let about: About?
    let coordinates: Coordinates?
    let team: Team?
    
    public init() {
        self.players = nil
        self.result = nil
        self.about = nil
        self.coordinates = nil
        self.team = nil
    }
}

// MARK: - CurrentPlayResult
public struct CurrentPlayResult : Codable {
    let event: String
    let eventCode: String
    let eventTypeId: String
    let description: String
}

// MARK: - PlaysByPeriod
public struct PlaysByPeriod : Codable {
    let startIndex: Int
    let plays: [Int]
    let endIndex: Int
}

// MARK: - MetaData
public struct MetaData : Codable {
    let wait: Int
    let timeStamp: String
    
    public init() {
        self.wait = 0
        self.timeStamp = ""
    }
}
