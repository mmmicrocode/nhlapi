//
//  Person.swift
//  
//
//  Created by Steven Heil on 12/6/21.
//

import Foundation

// MARK: - Person
public struct Person: Codable {
    public let copyright: String
    public let people: [PersonDetail]
}

// MARK: - PersonDetail
public struct PersonDetail: Codable {
    public let id: Int
    public let fullName, link, firstName, lastName: String?
    public let primaryNumber, birthDate: String?
    public let currentAge: Int?
    public let birthCity, birthStateProvince, birthCountry, nationality, height: String?
    public let weight: Int?
    public let active, alternateCaptain, captain, rookie: Bool?
    public let shootsCatches, rosterStatus: String?
    public let currentTeam: CurrentTeam?
    public let primaryPosition: Position?
    public let stats: [StatElement]?
}

// MARK: - CurrentTeam
public struct CurrentTeam: Codable {
    public let id: Int?
    public let name, link: String
}

// MARK: - IndividualStats
public struct IndividualStats: Codable {
    public let copyright: String
    public let stats: [StatElement]
}

// MARK: - StatElement
public struct StatElement: Codable {
    public let type: TypeString
    public let splits: [Split]
}

// MARK: - Split
public struct Split: Codable {
    public let season: String?
    public let stat: SplitStat?
    public let team: CurrentTeam?
    public let league: Simple?
    public let sequenceNumber: Int?
    
    public var displaySeason: String? {
        guard let sea = season, sea.count == 8 else { return nil }
        return sea.prefix(4) + "-" + sea.suffix(4)
    }
}

// MARK: - SplitStat
public struct SplitStat: Codable {
    public let timeOnIce: String?
    public let assists, goals, pim, shots: Int?
    public let games, hits, powerPlayGoals, powerPlayPoints: Int?
    public let powerPlayTimeOnIce, evenTimeOnIce, penaltyMinutes: String?
    public let faceOffPct, shotPct: Double?
    public let gameWinningGoals, overTimeGoals, shortHandedGoals, shortHandedPoints: Int?
    public let shortHandedTimeOnIce: String?
    public let blocked, plusMinus, points, shifts: Int?
    public let timeOnIcePerGame, evenTimeOnIcePerGame, shortHandedTimeOnIcePerGame, powerPlayTimeOnIcePerGame: String?
}

// MARK: - TypeClass
public struct TypeString: Codable {
    public let displayName: StatsType
    public let gameType: GameTypeComplex?
}

public struct GameTypeComplex: Codable {
    public let id: GameType
    public let description: String
    public let postseason: Bool
}

public enum StatsType: String, Codable {
    case yearByYear = "yearByYear"
    case yearByYearRank = "yearByYearRank"
    case yearByYearPlayoffs = "yearByYearPlayoffs"
    case yearByYearPlayoffsRank = "yearByYearPlayoffsRank"
    case careerRegularSeason = "careerRegularSeason"
    case careerPlayoffs = "careerPlayoffs"
    case gameLog = "gameLog"
    case playoffGameLog = "playoffGameLog"
    case vsTeam = "vsTeam"
    case vsTeamPlayoffs = "vsTeamPlayoffs"
    case vsDivision = "vsDivision"
    case vsDivisionPlayoffs = "vsDivisionPlayoffs"
    case vsConference = "vsConference"
    case vsConferencePlayoffs = "vsConferencePlayoffs"
    case byMonth = "byMonth"
    case byMonthPlayoffs = "byMonthPlayoffs"
    case byDayOfWeek = "byDayOfWeek"
    case byDayOfWeekPlayoffs = "byDayOfWeekPlayoffs"
    case homeAndAway = "homeAndAway"
    case homeAndAwayPlayoffs = "homeAndAwayPlayoffs"
    case winLoss = "winLoss"
    case winLossPlayoffs = "winLossPlayoffs"
    case onPaceRegularSeason = "onPaceRegularSeason"
    case regularSeasonStatRankings = "regularSeasonStatRankings"
    case playoffStatRankings = "playoffStatRankings"
    case goalsByGameSituation = "goalsByGameSituation"
    case goalsByGameSituationPlayoffs = "goalsByGameSituationPlayoffs"
    case statsSingleSeason = "statsSingleSeason"
    case statsSingleSeasonPlayoffs = "statsSingleSeasonPlayoffs"
}

