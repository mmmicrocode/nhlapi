//
//  Roster.swift
//  
//
//  Created by Steven Heil on 12/6/21.
//

import Foundation

// MARK: Roster
public struct Roster: Codable {
    public let copyright: String
    public let roster: [RosterElement]
    public let link: String

    public var rosterSortedByNumber : [RosterElement] {
        get {
            return self.roster.sorted {
                Int($0.jerseyNumber) ?? 0 < Int($1.jerseyNumber) ?? 0
            }
        }
    }
    
    public var rosterSortedByPosition : [RosterElement] {
        get {
            return self.roster.sorted {
                $0.position.code < $1.position.code
            }
        }
    }
    
    public var rosterSortedByName : [RosterElement] {
        get {
            return self.roster.sorted {
                $0.person.fullName < $1.person.fullName
            }
        }
    }
}

// MARK: - RosterElement
public struct RosterElement: Codable {
    public let person: RosterPerson
    public let jerseyNumber: String
    public let position: Position
    
    public init(person: RosterPerson, jerseyNum: String, position: Position) {
        self.person = person
        self.jerseyNumber = jerseyNum
        self.position = position
    }
}

// MARK: - Person
public struct RosterPerson: Codable {
    public let id: Int
    public let fullName, link: String
    
    public init(id: Int, fullName: String, link: String) {
        self.id = id
        self.fullName = fullName
        self.link = link
    }
}

// MARK: - Position
public struct Position: Codable {
    public let code, name: String
    public let type: PositionType
    public let abbreviation: String
    
    public init(code: String, name: String, type: PositionType, abbreviation: String) {
        self.code = code
        self.name = name
        self.type = type
        self.abbreviation = abbreviation
    }
}

public enum PositionType: String, Codable {
    case defenseman = "Defenseman"
    case forward = "Forward"
    case goalie = "Goalie"
}

