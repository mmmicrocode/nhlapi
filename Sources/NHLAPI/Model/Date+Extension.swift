//
//  Date+Extension.swift
//  
//
//  Created by Steven Heil on 12/6/21.
//

import Foundation

public extension Date {
    var nhlScheduleDateFormat : String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: self)
    }
    
    static var yesterday: Date { return Date().dayBefore }
    static var tomorrow:  Date { return Date().dayAfter }
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
    
    func daysFromToday(days: Int) -> Date {
        let calendar = Calendar.current
        let midnight = calendar.startOfDay(for: self)
        return calendar.date(byAdding: .day, value: days, to: midnight)!
    }
}

