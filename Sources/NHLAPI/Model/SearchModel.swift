// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let searchResults = try? newJSONDecoder().decode(SearchResults.self, from: jsonData)

import Foundation

// MARK: - SearchResults
public struct SearchResults: Codable {
    public let meta: Meta
    public let facets: [Facet]
    public let docs: [Doc]
    
    public init() {
        self.meta = Meta()
        self.facets = []
        self.docs = []
    }
}

// MARK: - Doc
public struct Doc: Codable {
    public let subhead: String?
    public let lastUpdated: Date
    public let image: Image
    public let contributor: Contributor
    public let titles: [String]
    public let duration: String
    public let bigBlurb, assetID: String?
    public let byline: String?
    public let title: String
    public let tagline, partnerBroadcastID, distributionContextID: String?
    public let type: TypeEnum
    public let blurb: String?
    public let lang: String
    public let altLanguages: AltLanguages
    public let authFlow: Bool
    public let tags: [TagOrKeyword]
    public let displayTimestamp, firstPublished, sortTimestamp: Date
    public let seoTitle: String
    public let langs: [String]
    public let bylines: [String]
    public let slug: String
    public let seoKeywords, mediaPlaybackURL: String?
    public let commenting: Bool?
    public let keywordsDisplay: [TagOrKeyword]
    public let url: String
    public let broadcastID, presentationContextID: String?
    public let flags: [String]
    public let seoDescription: String?

    enum CodingKeys: String, CodingKey {
        case subhead
        case lastUpdated = "last_updated"
        case image, contributor, titles, duration, bigBlurb
        case assetID = "asset_id"
        case byline, title, tagline
        case partnerBroadcastID = "partnerBroadcastId"
        case distributionContextID = "distributionContextId"
        case type, blurb, lang, altLanguages, authFlow, tags
        case displayTimestamp = "display_timestamp"
        case firstPublished = "first_published"
        case sortTimestamp = "sort_timestamp"
        case seoTitle, langs, bylines, slug, seoKeywords, mediaPlaybackURL, commenting, keywordsDisplay, url
        case broadcastID = "broadcastId"
        case presentationContextID = "presentationContextId"
        case flags, seoDescription
    }
}

// MARK: - AltLanguages
public struct AltLanguages: Codable {
    let fr, ru, de, sv, sk, cs, fi, es: DocDetails?
}

// MARK: - DocDetails
public struct DocDetails: Codable {
    let headline: String
    let url: String
    let bigBlurb, blurb, slug: String
}

// MARK: - Contributor
public struct Contributor: Codable {
}

// MARK: - Image
public struct Image: Codable {
    let cuts: [String: Cut]?
    let altText, title: String?
}

// MARK: - Cut
public struct Cut: Codable {
    let src: String
    let height, width: Int
    let at2x: String
    let aspectRatio: String
    let at3x: String
}

// MARK: - TagOrKeyword
public struct TagOrKeyword: Codable {
    let displayName: String
    let type: String
    let value: String
}

public enum TypeEnum: String, Codable {
    case all = "all"
    case article = "article"
    case video = "video"
    case topicList = "topicList"
}

// MARK: - Facet
public struct Facet: Codable {
    let field: String
    let values: [Value]
}

// MARK: - Value
public struct Value: Codable {
    let count: Int
    let value: TypeEnum
}

// MARK: - Meta
public struct Meta: Codable {
    let hits, time, pageSize, offset: Int

    enum CodingKeys: String, CodingKey {
        case hits, time
        case pageSize = "page_size"
        case offset
    }
    
    public init() {
        self.hits = 0
        self.time = 0
        self.pageSize = 0
        self.offset = 0
    }
}
