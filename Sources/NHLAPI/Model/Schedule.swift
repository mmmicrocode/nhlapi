//
//  Schedule.swift
//  
//
//  Created by Steven Heil on 12/6/21.
//

import Foundation

// MARK: Schedule
public struct Schedule : Codable {
    public var copyright: String
    public var totalItems, totalEvents, totalGames, totalMatches, wait: Int
    public var dates: [ScheduleDate]
    
    public init() {
        self.copyright = ""
        self.totalItems = 0
        self.totalEvents = 0
        self.totalGames = 0
        self.totalMatches = 0
        self.wait = 10
        self.dates = [ScheduleDate]()
    }
}

// MARK: - ScheduleDate
public struct ScheduleDate : Codable {
    public let date: Date
    public let totalItems, totalEvents, totalGames, totalMatches: Int
    public let games: [Game]
    //let events, matches: []?
    
    private enum CodingKeys: String, CodingKey {
         case date, totalItems, totalEvents, totalGames, totalMatches, games
     }
}

// MARK: - Game
public struct Game : Codable, Identifiable {
    public var gameId: Int
    public var link : String?
    public var gameType : GameType
    public var season: String
    public var gameDate: Date
    public var status: GameStatus
    public var teams: GameTeams
    public var venue: Venue
    public var content: Simple?
    
    public var id: Int { gameId }
    
    public enum CodingKeys: String, CodingKey {
        case gameId = "gamePk"
        case link, gameType, season, gameDate, status, teams, venue, content
     }
    
    public init(gameId: Int, link: String?, gameType: GameType, season: String, gameDate: Date, status: GameStatus, teams: GameTeams, venue: Venue, content: Simple?) {
        self.gameId = gameId
        self.link = link
        self.gameType = gameType
        self.season = season
        self.gameDate = gameDate
        self.status = status
        self.teams = teams
        self.venue = venue
        self.content = content
    }
}

public enum GameType : String, Codable {
    case preseasion = "PR"
    case regularSeason = "R"
    case playoff = "P"
}

// MARK: - Status
public struct GameStatus : Codable {
    public var abstractGameState : GameStateType
    public var codedGameState: String?
    public var detailedState : DetailedGameStateType
    public var statusCode: String?
    public var startTimeTBD: Bool?
    
    public init() {
        self.abstractGameState = .final
        self.codedGameState = "Final"
        self.detailedState = .finalFinal
        self.statusCode = ""
        self.startTimeTBD = false
    }
    
    public init(abstractGameState: GameStateType, codedGameState: String?, detailedState: DetailedGameStateType, statusCode: String?, startTimeTBD: Bool?) {
        self.abstractGameState = abstractGameState
        self.codedGameState = codedGameState
        self.detailedState = detailedState
        self.statusCode = statusCode
        self.startTimeTBD = startTimeTBD
    }
}

public enum GameStateType : String, Codable {
    case preview = "Preview"
    case final = "Final"
    case live = "Live"
}
public enum DetailedGameStateType : String, Codable {
    case previewScheduled = "Scheduled"             // 1
    case previewPregame = "Pre-Game"                // 2
    case liveInProgress = "In Progress"             // 3
    case liveCritical = "In Progress - Critical"    // 4
    case finalGameOver = "Game Over"                // 5
    case finalFinal = "Final"                       // 6 or 7
    case previewPostponed = "Postponed"
}

// MARK: - GameTeams
public struct GameTeams : Codable {
    public var away : GameTeamAndScore
    public var home : GameTeamAndScore
    
    public init(away: GameTeamAndScore, home: GameTeamAndScore) {
        self.away = away
        self.home = home
    }
}

// MARK: - GameTeamAndScore
public struct GameTeamAndScore : Codable {
    public var leagueRecord : LeagueRecord?
    public var score : Int?
    public var team : Team?
    
    public init() {
        self.leagueRecord = nil
        self.score = nil
        self.team = nil
    }
    
    public init(leagueRecord: LeagueRecord, score: Int, team: Team) {
        self.leagueRecord = leagueRecord
        self.score = score
        self.team = team
    }
}

// MARK: - Venue
public struct Venue : Codable {
    public var id: Int?
    public var name: String?
    public var link, city: String?
    public var timeZone: TimeZone?
    
    public init() {
        self.id = 0
    }
    
    public init(id: Int?, name: String?, link: String?, city: String?, timeZone: TimeZone?) {
        self.id = id
        self.name = name
        self.link = link
        self.city = city
        self.timeZone = timeZone
    }
}

// MARK: - TimeZone
public struct TimeZone : Codable {
    public let id: String?
    public let offset: Int?
    public let tz: String?
}
