//
//  Division.swift
//  
//
//  Created by Steven Heil on 12/6/21.
//

import Foundation

// MARK: - Division
public struct Division : Codable, Identifiable {
    public let id: Int?
    public let name, nameShort, link, abbreviation: String?
    
    public static func nickname(for id: Int) -> String {
        switch(id) {
        case 15: return "Pacific"
        case 16: return "Central"
        case 17: return "Atlantic"
        case 18: return "Metropolitan"
        default: return ""
        }
    }
    public var nickname : String {
        guard let id = self.id else { return "" }
        return Division.nickname(for: id)
    }
    public static func nicknameShort(for id: Int) -> String {
        switch(id) {
        case 15: return "PAC"
        case 16: return "CEN"
        case 17: return "ATL"
        case 18: return "Metro"
        default: return ""
        }
    }
    public var nicknameShort: String {
        guard let id = self.id else { return "" }
        return Division.nicknameShort(for: id)
    }
}

// MARK: - Conference
public struct Conference : Codable {
    public let id: Int?
    public let name, link: String?
    
    public var nickname : String {
        switch(self.id) {
        case 5: return "Western"
        case 6: return "Eastern"
        default: return ""
        }
    }
}

