//
//  JSONDecoder+Extension.swift
//  
//
//  Created by Steven Heil on 12/6/21.
//

import Foundation

extension JSONDecoder.DateDecodingStrategy {
    static let nhlapiDateDecodingStrategy = custom {
        let container = try $0.singleValueContainer()
        let string = try container.decode(String.self)
        if let date = Formatter.nhlapiShortDate.date(from: string)
                    ?? Formatter.nhlapiGameDate.date(from: string) {
            return date
        }
        throw DecodingError.dataCorruptedError(in: container, debugDescription: "Invalid date: \(string)")
    }
}
