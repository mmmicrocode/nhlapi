//
//  TeamDisplay.swift
//  
//
//  Created by Steven Heil on 12/6/21.
//

import Foundation

public enum TeamDisplay: Int, Codable {
    case undefined = 0
    case devils = 1
    case islanders = 2
    case rangers = 3
    case flyers = 4
    case penguins = 5
    case bruins = 6
    case sabres = 7
    case canadiens = 8
    case senators = 9
    case mapleLeafs = 10
    case hurricanes = 12
    case panthers = 13
    case lightning = 14
    case capitals = 15
    case blackhawks = 16
    case redWings = 17
    case predators = 18
    case blues = 19
    case flames = 20
    case avalanche = 21
    case oilers = 22
    case canucks = 23
    case ducks = 24
    case stars = 25
    case kings = 26
    
    case sharks = 28
    case blueJackets = 29
    case wild = 30
    
    case jets = 52
    case coyotes = 53
    case goldenKnights = 54
    case kraken = 55
    
    init(fromRawValue: Int) {
        self = TeamDisplay(rawValue: fromRawValue) ?? .kraken
    }
    
    public var nickname: String {
        switch(self) {
        case .undefined: return "UNDEFINED"
        case .capitals: return "Capitals"
        case .hurricanes: return "Hurricanes"
        case .penguins: return "Penguins"
        case .islanders: return "Islanders"
        case .blueJackets: return "Blue Jackets"
        case .flyers: return "Flyers"
        case .devils: return "Devils"
        case .rangers: return "Rangers"
        case .sabres: return "Sabres"
        case .bruins: return "Bruins"
        case .mapleLeafs: return "Maple Leafs"
        case .lightning: return "Lightning"
        case .panthers: return "Panthers"
        case .canadiens: return "Canadiens"
        case .redWings: return "Red Wings"
        case .senators: return "Senators"
        case .avalanche: return "Avalanche"
        case .predators: return "Predators"
        case .blues: return "Blues"
        case .jets: return "Jets"
        case .stars: return "Stars"
        case .blackhawks: return "Blackhawks"
        case .wild: return "Wild"
        case .oilers: return "Oilers"
        case .goldenKnights: return "Golden Knights"
        case .canucks: return "Canucks"
        case .ducks: return "Ducks"
        case .coyotes: return "Coyotes"
        case .flames: return "Flames"
        case .kings: return "Kings"
        case .sharks: return "Sharks"
        case .kraken: return "Kraken"
        }
    }

    public var city: String {
        switch(self) {
        case .undefined: return "UNDEFINED"
        case .capitals: return "Washington"
        case .hurricanes: return "Carolina"
        case .penguins: return "Pittsburgh"
        case .islanders: return "New York"
        case .blueJackets: return "Columbus"
        case .flyers: return "Philadelphia"
        case .devils: return "New Jersey"
        case .rangers: return "New York"
        case .sabres: return "Buffalo"
        case .bruins: return "Boston"
        case .mapleLeafs: return "Toronto"
        case .lightning: return "Tampa Bay"
        case .panthers: return "Florida"
        case .canadiens: return "Montréal"
        case .redWings: return "Detroit"
        case .senators: return "Ottawa"
        case .avalanche: return "Colorado"
        case .predators: return "Nashville"
        case .blues: return "St. Louis"
        case .jets: return "Winnipeg"
        case .stars: return "Dallas"
        case .blackhawks: return "Chicago"
        case .wild: return "Minnesota"
        case .oilers: return "Edmonton"
        case .goldenKnights: return "Vegas"
        case .canucks: return "Vancouver"
        case .ducks: return "Anaheim"
        case .coyotes: return "Arizona"
        case .flames: return "Calgary"
        case .kings: return "Los Angeles"
        case .sharks: return "San Jose"
        case .kraken: return "Seattle"
        }
    }
    
    public var abbreviation: String {
        switch(self) {
        case .undefined: return "UND"
        case .capitals: return "WSH"
        case .hurricanes: return "CAR"
        case .penguins: return "PIT"
        case .islanders: return "NYI"
        case .blueJackets: return "CBJ"
        case .flyers: return "PHI"
        case .devils: return "NJD"
        case .rangers: return "NYR"
        case .sabres: return "BUF"
        case .bruins: return "BOS"
        case .mapleLeafs: return "TOR"
        case .lightning: return "TBL"
        case .panthers: return "FLA"
        case .canadiens: return "MTL"
        case .redWings: return "DET"
        case .senators: return "OTT"
        case .avalanche: return "COL"
        case .predators: return "NSH"
        case .blues: return "STL"
        case .jets: return "WPG"
        case .stars: return "DAL"
        case .blackhawks: return "CHI"
        case .wild: return "MIN"
        case .oilers: return "EDM"
        case .goldenKnights: return "VGK"
        case .canucks: return "VAN"
        case .ducks: return "ANH"
        case .coyotes: return "ARI"
        case .flames: return "CGY"
        case .kings: return "LAK"
        case .sharks: return "SJS"
        case .kraken: return "SEA"
        }
    }
}
