//
//  Standings.swift
//  
//
//  Created by Steven Heil on 12/6/21.
//

import Foundation

// MARK: Standings
public struct Standings : Codable {
    public let copyright: String
    public let records: [Record]
    public var timestamp: Date?
    
    private enum StandingsCodingKey: String, CodingKey {
        case copyright, records
    }
    
    public init(copyright: String, records: [Record], timestamp: Date?) {
        self.copyright = copyright
        self.records = records
        self.timestamp = timestamp
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: StandingsCodingKey.self)
        self.copyright = try container.decode(String.self, forKey: .copyright)
        self.records = try container.decode([Record].self, forKey: .records)
        self.timestamp = Date()
    }
}

// MARK: - Record
public enum StandingsType : String, Codable {
    case regularSeason = "regularSeason"
    case wildCard = "wildCard"
    case byConference = "byConference"
    case byDivision = "byDivision"
    case divisionLeaders = "divisionLeaders"
}

public struct Record : Codable, Identifiable {
    public var id: String
    public let standingsType: StandingsType
    public let league: Simple
    public var division: Expanded?
    public var conference: Simple?
    public let teamRecords: [TeamRecord]
    
    public init(standingsType: StandingsType, league: Simple, division: Expanded?, conference: Simple?, teamRecords: [TeamRecord]) {
        self.id = UUID().uuidString
        self.standingsType = standingsType
        self.league = league
        self.division = division
        self.conference = conference
        self.teamRecords = teamRecords
    }
    
    private enum RecordCodingKey: CodingKey {
        case id
        case standingsType
        case league
        case division
        case conference
        case teamRecords
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: RecordCodingKey.self)
        self.id = UUID().uuidString
        self.standingsType = try container.decode(StandingsType.self, forKey: .standingsType)
        self.league = try container.decode(Simple.self, forKey: .league)
        self.division = try container.decodeIfPresent(Expanded.self, forKey: .division)
        self.conference = try container.decodeIfPresent(Simple.self, forKey: .conference)
        self.teamRecords = try container.decode([TeamRecord].self, forKey: .teamRecords)
    }
}

// MARK: - Simple
public struct Simple : Codable {
    public let id: Int?
    public let name, link: String?
}

// MARK: - Expanded
public struct Expanded : Codable {
    public let id: Int
    public let name, nameShort, link, abbreviation: String?
    
    public var nameInitial : String {
        if let short = self.nameShort,
           let shortFirst = short.first {
            return String(shortFirst)
        }
        else {
            return "-"
        }
    }
}

public struct TeamRecord : Codable {
    public let team: Team
    public let leagueRecord: LeagueRecord
    public let goalsAgainst, goalsScored, points: Int
    public let divisionRank, divisionL10Rank, divisionRoadRank, divisionHomeRank: String
    public let conferenceRank, conferenceL10Rank, conferenceRoadRank, conferenceHomeRank: String
    public let leagueRank, leagueL10Rank, leagueRoadRank, leagueHomeRank: String
    public let wildCardRank: String
    public let row, gamesPlayed: Int
    public let streak: Streak?
    public let lastUpdated: Date
    
    public var divisionRankRelative : String {
        switch(self.divisionRank) {
        case "1":
            return "1st"
        case "2":
            return "2nd"
        case "3":
            return "3rd"
        case "4":
            return "4th"
        case "5":
            return "5th"
        case "6":
            return "6th"
        case "7":
            return "7th"
        case "8":
            return "8th"
        case "9":
            return "9th"
        case "10":
            return "10th"
        default:
            return self.divisionRank
        }
    }

    public var playoffTeam: Bool {
        return Int(wildCardRank) ?? 99 < 3
    }
}

extension TeamRecord: Hashable, Equatable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(team.id)
    }
    public static func == (lhs: TeamRecord, rhs: TeamRecord) -> Bool {
        return lhs.team.id == rhs.team.id
    }
}

// MARK: - LeagueRecord
public struct LeagueRecord : Codable {
    public let wins, losses, ot: Int
    public let type: String
    
    public init(wins: Int, losses: Int, ot: Int, type: String) {
        self.wins = wins
        self.losses = losses
        self.ot = ot
        self.type = type
    }
}

// MARK: - Streak
public struct Streak : Codable {
    public let streakType: StreakType
    public let streakNumber: Int
    public let streakCode: String
}

public enum StreakType : String, Codable {
    case losses = "losses"
    case ot = "ot"
    case wins = "wins"
}
