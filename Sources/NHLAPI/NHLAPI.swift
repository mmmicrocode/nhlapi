//
//  NHLAPI.swift
//
//  Created by Steve Heil on 10/30/19.
//

import Foundation
import Combine
import UserNotifications

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 5.0, *)
public struct NHLAPI {
    static let apiUrlStandingsByDivision = "https://statsapi.web.nhl.com/api/v1/standings/byDivision"
    static let apiUrlStandingsByConference = "https://statsapi.web.nhl.com/api/v1/standings/byConference"
    static let apiUrlStandingsByWildCard = "https://statsapi.web.nhl.com/api/v1/standings/wildCardWithLeaders"
    
    static let apiUrlSchedule = "https://statsapi.web.nhl.com/api/v1/schedule"
    static let apiUrlScheduleForTeam = "https://statsapi.web.nhl.com/api/v1/schedule?teamId="
                                        //5&startDate=2021-11-07&endDate=2021-11-14"
    
    static let apiUrlTeamDetails = "https://statsapi.web.nhl.com/api/v1/teams/"
    static let apiUrlTeamRoster = "https://statsapi.web.nhl.com/api/v1/teams/"
    
    static let apiUrlPersonDetails = "https://statsapi.web.nhl.com/api/v1/people/"
    static let apiUrlPersonStats = "https://statsapi.web.nhl.com/api/v1/people/"
    static let apiUrlPersonStatsSingleSeasonSuffix = "/stats?stats=statsSingleSeason&season="  //20192020
    static let apiUrlPersonDetailsExpandedSuffix = "?expand=person.stats&stats=yearByYear,yearByYearPlayoffs,careerRegularSeason&expand=stats.team"
    
    static let apiUrlGame = "https://statsapi.web.nhl.com/api/v1/game/"
    static let apiUrlGameContentSuffix = "/content"
    static let apiUrlGameLiveFeedSuffix = "/feed/live"
    
    static let apiUrlSearchRoot = "https://search-api.svc.nhl.com/svc/search/v2/nhl_global_sitesearch_en/sitesearch?hl=true&facet=type&page=1"
    
    var session : URLSession
    
    public init(withSession: URLSession = .shared) {
        session = withSession
    }

    //
    //MARK: - Division Standings
    //
    @available(*, renamed: "getNHLStandingsByDivision()")
    public func getNHLStandingsByDivision(completion: @escaping (Result<Standings,APIError>) -> Void) {
        Task {
            do {
                let result = try await getNHLStandingsByDivision()
                completion(.success(result))
            } catch {
                completion(.failure(error as! APIError))
            }
        }
    }
    
    public func getNHLStandingsByDivision() async throws -> Standings {
        guard let divUrl = URL(string: NHLAPI.apiUrlStandingsByDivision) else { return Standings(copyright: "", records: [], timestamp: nil) }
        
        return try await withCheckedThrowingContinuation { continuation in
            let task = session.dataTask(with: divUrl) { data, _, error in
                guard let data1 = data else { return }
                
                do {
                    let result = try NHLAPI.nhlDecodeStrategyDecoder.decode(Standings.self, from: data1)
                    continuation.resume(with: .success(result))
                }
                catch {
                    print("Parse response error: \(error)")
                    continuation.resume(with: .failure(APIError.UnknownError))
                }
            }
            task.resume()
        }
    }

    //
    //MARK: - Conference Standings
    //
    public func getNHLStandingsByConference() async throws -> Standings {
        guard let divUrl = URL(string: NHLAPI.apiUrlStandingsByConference) else { return Standings(copyright: "", records: [], timestamp: nil) }
        
        return try await withCheckedThrowingContinuation { continuation in
            let task = session.dataTask(with: divUrl) { data, _, error in
                guard let data1 = data else { return }
                
                do {
                    let result = try NHLAPI.nhlDecodeStrategyDecoder.decode(Standings.self, from: data1)
                    continuation.resume(with: .success(result))
                }
                catch {
                    print("Parse response error: \(error)")
                    continuation.resume(with: .failure(APIError.UnknownError))
                }
            }
            task.resume()
        }
    }

    //
    //MARK: - Wildcard Standings
    //
    public func getNHLStandingsByWildcard() async throws -> Standings {
        guard let divUrl = URL(string: NHLAPI.apiUrlStandingsByWildCard) else { return Standings(copyright: "", records: [], timestamp: nil) }
        
        return try await withCheckedThrowingContinuation { continuation in
            let task = session.dataTask(with: divUrl) { data, _, error in
                guard let data1 = data else { return }
                
                do {
                    let result = try NHLAPI.nhlDecodeStrategyDecoder.decode(Standings.self, from: data1)
                    continuation.resume(with: .success(result))
                }
                catch {
                    print("Parse response error: \(error)")
                    continuation.resume(with: .failure(APIError.UnknownError))
                }
            }
            task.resume()
        }
    }

    //
    //MARK: - Today Schedule
    //
    public func getNHLTodaySchedule() async throws -> Schedule {
        guard let scheduleUrl = URL(string: NHLAPI.apiUrlSchedule) else { return Schedule() }
        
        return try await withCheckedThrowingContinuation { continuation in
            let task = session.dataTask(with: scheduleUrl) { data, _, error in
                guard let data1 = data else { return }
                
                do {
                    let result = try NHLAPI.nhlDecodeStrategyDecoder.decode(Schedule.self, from: data1)
                    continuation.resume(with: .success(result))
                }
                catch {
                    print("Parse response error: \(error)")
                    continuation.resume(with: .failure(APIError.UnknownError))
                }
            }
            task.resume()
        }
    }

    //
    //MARK: - Schedule with Date Range
    //
    public func getNHLTodaySchedule(startDate: Date, endDate: Date) async throws -> Schedule {
        let scheduleUrlString = String("\(NHLAPI.apiUrlSchedule)?startDate=\(startDate.nhlScheduleDateFormat)&endDate=\(endDate.nhlScheduleDateFormat)")
        guard let scheduleUrl = URL(string: scheduleUrlString) else { return Schedule() }

        return try await withCheckedThrowingContinuation { continuation in
            let task = session.dataTask(with: scheduleUrl) { data, _, error in
                guard let data1 = data else { return }
                
                do {
                    let result = try NHLAPI.nhlDecodeStrategyDecoder.decode(Schedule.self, from: data1)
                    continuation.resume(with: .success(result))
                }
                catch {
                    print("Parse response error: \(error)")
                    continuation.resume(with: .failure(APIError.UnknownError))
                }
            }
            task.resume()
        }
    }

    //
    //MARK: - Schedule Range for Team
    //
    public func getNHLScheduleRangeForTeam(teamId: Int, startDate: Date, endDate: Date) async throws -> Schedule {
        let scheduleUrlString = String("\(NHLAPI.apiUrlScheduleForTeam)\(teamId)&startDate=\(startDate.nhlScheduleDateFormat)&endDate=\(endDate.nhlScheduleDateFormat)")
        guard let scheduleUrl = URL(string: scheduleUrlString) else { return Schedule() }

        return try await withCheckedThrowingContinuation { continuation in
            let task = session.dataTask(with: scheduleUrl) { data, _, error in
                guard let data1 = data else { return }
                
                do {
                    let result = try NHLAPI.nhlDecodeStrategyDecoder.decode(Schedule.self, from: data1)
                    continuation.resume(with: .success(result))
                }
                catch {
                    print("Parse response error: \(error)")
                    continuation.resume(with: .failure(APIError.UnknownError))
                }
            }
            task.resume()
        }
    }

    //
    //MARK: - Team Details
    //
    public func getNHLTeamDetails(teamId: Int) async throws -> TeamDetail {
        let teamUrlString = String("\(NHLAPI.apiUrlTeamDetails)\(teamId)")
        guard let teamUrl = URL(string: teamUrlString) else { return TeamDetail(copyright: nil, teams: []) }

        return try await withCheckedThrowingContinuation { continuation in
            let task = session.dataTask(with: teamUrl) { data, _, error in
                guard let data1 = data else { return }
                
                do {
                    let result = try NHLAPI.nhlDecodeStrategyDecoder.decode(TeamDetail.self, from: data1)
                    continuation.resume(with: .success(result))
                }
                catch {
                    print("Parse response error: \(error)")
                    continuation.resume(with: .failure(APIError.UnknownError))
                }
            }
            task.resume()
        }
    }

    //
    //MARK: - Team Roster
    //
    public func getNHLTeamRoster(teamId: Int) async throws -> Roster {
        let teamUrlString = String("\(NHLAPI.apiUrlTeamRoster)\(teamId)/roster")
        guard let teamUrl = URL(string: teamUrlString) else { return Roster(copyright: "", roster: [], link: "") }

        return try await withCheckedThrowingContinuation { continuation in
            let task = session.dataTask(with: teamUrl) { data, _, error in
                guard let data1 = data else { return }
                
                do {
                    let result = try NHLAPI.nhlDecodeStrategyDecoder.decode(Roster.self, from: data1)
                    continuation.resume(with: .success(result))
                }
                catch {
                    print("Parse response error: \(error)")
                    continuation.resume(with: .failure(APIError.UnknownError))
                }
            }
            task.resume()
        }
    }

    //
    //MARK: - Person Details
    //
    public func getNHLPersonDetails(personId: Int) async throws -> Person {
        let personUrlString = String("\(NHLAPI.apiUrlPersonDetails)\(personId)\(NHLAPI.apiUrlPersonDetailsExpandedSuffix)")
        guard let personUrl = URL(string: personUrlString) else { return Person(copyright: "", people: []) }

        return try await withCheckedThrowingContinuation { continuation in
            let task = session.dataTask(with: personUrl) { data, _, error in
                guard let data1 = data else { return }
                
                do {
                    let result = try NHLAPI.nhlDecodeStrategyDecoder.decode(Person.self, from: data1)
                    continuation.resume(with: .success(result))
                }
                catch {
                    print("Parse response error: \(error)")
                    continuation.resume(with: .failure(APIError.UnknownError))
                }
            }
            task.resume()
        }
    }

    //
    //MARK: - Person Stats Single Season
    //
    public func getNHLPersonStatsSingleSeason(personId: Int, season: String) async throws -> IndividualStats {
        let personUrlString = String("\(NHLAPI.apiUrlPersonStats)\(personId)\(NHLAPI.apiUrlPersonStatsSingleSeasonSuffix)\(season)")
        guard let personUrl = URL(string: personUrlString) else { return IndividualStats(copyright: "", stats: []) }

        return try await withCheckedThrowingContinuation { continuation in
            let task = session.dataTask(with: personUrl) { data, _, error in
                guard let data1 = data else { return }
                
                do {
                    let result = try NHLAPI.nhlDecodeStrategyDecoder.decode(IndividualStats.self, from: data1)
                    continuation.resume(with: .success(result))
                }
                catch {
                    print("Parse response error: \(error)")
                    continuation.resume(with: .failure(APIError.UnknownError))
                }
            }
            task.resume()
        }
    }

    //
    //MARK: - Search
    //
    public func getNHLSearchResults(searchTerm: String, type: TypeEnum = .all) async throws -> SearchResults {
        let term = searchTerm.replacingOccurrences(of: " ", with: "+")
        let searchUrlString = String("\(NHLAPI.apiUrlSearchRoot)&q=\(term)")
        guard let searchUrl = URL(string: searchUrlString) else { return SearchResults() }

        return try await withCheckedThrowingContinuation { continuation in
            let task = session.dataTask(with: searchUrl) { data, _, error in
                guard let data1 = data else { return }
                
                do {
                    let result = try NHLAPI.nhlDecodeStrategyDecoder.decode(SearchResults.self, from: data1)
                    continuation.resume(with: .success(result))
                }
                catch {
                    print("Parse response error: \(error)")
                    continuation.resume(with: .failure(APIError.UnknownError))
                }
            }
            task.resume()
        }
    }

    //
    //MARK: - Game Content
    //
    public func getNHLGameContent(gameId: Int) async throws -> GameContent {
        let contentUrlString = String("\(NHLAPI.apiUrlGame)\(gameId)\(NHLAPI.apiUrlGameContentSuffix)")
        guard let contentUrl = URL(string: contentUrlString) else { return GameContent() }

        return try await withCheckedThrowingContinuation { continuation in
            let task = session.dataTask(with: contentUrl) { data, _, error in
                guard let data1 = data else { return }
                
                do {
                    let result = try NHLAPI.nhlDecodeStrategyDecoder.decode(GameContent.self, from: data1)
                    continuation.resume(with: .success(result))
                }
                catch {
                    print("Parse response error: \(error)")
                    continuation.resume(with: .failure(APIError.UnknownError))
                }
            }
            task.resume()
        }
    }

    //
    //MARK: - Game Live Feed
    //
    public func getNHLGameLiveFeed(gameId: Int) async throws -> GameLiveFeed {
        let feedUrlString = String("\(NHLAPI.apiUrlGame)\(gameId)\(NHLAPI.apiUrlGameLiveFeedSuffix)")
        guard let feedUrl = URL(string: feedUrlString) else { return GameLiveFeed() }

        return try await withCheckedThrowingContinuation { continuation in
            let task = session.dataTask(with: feedUrl) { data, _, error in
                guard let data1 = data else { return }

                do {
                    let result = try NHLAPI.nhlDecodeStrategyDecoder.decode(GameLiveFeed.self, from: data1)
                    continuation.resume(with: .success(result))
                }
                catch {
                    print("Parse response error: \(error)")
                    continuation.resume(with: .failure(APIError.UnknownError))
                }
            }
            task.resume()
        }
    }

    //MARK: - Custom JSON Decoder
    
    static var nhlDecodeStrategyDecoder : JSONDecoder {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .nhlapiDateDecodingStrategy
        return decoder
    }
}
